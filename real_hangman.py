print("Hangman")
hangmanpics = ['''
   ______
   |    |
        |
        |
        |
        |
============''','''
   ______
   |    |
   O    |
        |
        |
        |
============''','''
   ______
   |    |
   O    |
   |    |
        |
        |
============''','''
   ______
   |    |
   O    |
  /|    |
        |
        |
============''','''
   ______
   |    |
   O    |
  /|\   |
        |
        |
============''','''
   ______
   |    |
   O    |
  /|\   |
  /     |
        |
============''','''
   ______
   |    |
   O    |
  /|\   |
  / \   |
        |
============''']
cities = ['chicago','beijing','milwaukee','lisbon','london','brussels','atlanta','barcelona','bangkok']
b = len(cities) - 1
from random import randint
secret_word = cities[randint(0,b)]
length = len(secret_word)
secret_list = list(secret_word)
guessed = []
already_used = []
indexes = []
tries = 0
hangman_numero = 0
for letter in secret_word:
    print("*", end=" ")
    guessed.append("*")
while guessed != secret_list:
    userguess = input("Guess the letter of the city: ")
    if userguess in already_used:
        print("You have already guessed that. Please guess another letter")
    else:
        already_used.append(userguess)
        if userguess in secret_word:
            for i, j in enumerate(secret_list):
                if j == userguess:
                    indexes.append(i)
            for index in indexes:
                del guessed[index];
                guessed.insert(index,userguess)
            print("Correct!")
        else:
            hangman_numero = hangman_numero + 1
            if hangman_numero <= 5:
                print("Incorrect. Try again.")
                print(hangmanpics[hangman_numero])
            else:
                print(hangmanpics[6])
                print("You lose.")
                print("The correct answer was: ")
                for thang in secret_list:
                    print(thang, end=" ")
                exit()
    for letter in guessed:
        print(letter, end=" ")
    indexes.clear()
    tries = tries + 1
print("You win!!")